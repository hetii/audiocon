Modified version of I2C driver for various FM radio tuners.
 
Please keep in mind that modules like 
videodev, v4l2-common, tuner, i2c_dev should be loaded.
 
You can also provide i2c bus number where chip is connected by: insmod audiocon.ko i2c_bus_nr=xxx

# Dmesg:
1. tea5767 8-0060: type set to Philips TEA5767HN FM Radio
2. tuner 8-0060: Tuner 62 found with type(s) Radio.
3. audiocon: V4L2 device registered as radio0
4. audiocon: 0.0.1: Successful initialized audiocon radio interface ;>

# LSMOD:
1. audiocon               13337  0 
2. tuner                  27381  1 
3. v4l2_common            16420  2 audiocon,tuner
4. tea5767                13253  1 
5. videodev              120309  3 audiocon,tuner,v4l2_common
6. i2c_dev                13370  0 
7. i2c_gpio               12803  0 
8. i2c_tiny_usb           12918  8 
9. i2c_algo_bit           13413  2 i2c_gpio,i915

![alt text](https://bitbucket.org/hetii/audiocon/src/16c2126f6ecf9f59949588d1d622a6b6df320091/audiocon.jpg "Audiocon")