fn = audiocon
obj-m += $(fn).o

all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
	@insmod $(fn).ko i2c_bus_nr=8 && sleep 1 && tail /var/log/syslog -n 4
	@lsmod |egrep 'tuner|audiocon|i2c|tea'
	@modinfo audiocon.ko

clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
	@rmmod $(fn).ko && tail /var/log/syslog -n 4
