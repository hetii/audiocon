
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/i2c.h>
#include <linux/videodev2.h>
#include <linux/slab.h>
#include <media/v4l2-device.h>
#include <media/v4l2-ioctl.h>
#include <media/v4l2-common.h>

#define DRIVER_VERSION  "0.0.1"
#define DRIVER_AUTHOR   "Grzegorz Hetman <ghetman@gmail.com>"
#define DRIVER_DESC     "Radio interface device for v4l2 based on i2c interface. Special thx for Dražen Lazarević that invented name for this driver :)"

#define PINFO(format, ...)\
        printk(KERN_INFO KBUILD_MODNAME ": "\
                DRIVER_VERSION ": " format "\n", ## __VA_ARGS__)
#define PWARN(format, ...)\
        printk(KERN_WARNING KBUILD_MODNAME ": "\
                DRIVER_VERSION ": " format "\n", ## __VA_ARGS__)
# define PDEBUG(format, ...)\
        printk(KERN_DEBUG KBUILD_MODNAME ": "\
                DRIVER_VERSION ": " format "\n", ## __VA_ARGS__)

static int i2c_bus_nr = 0;
module_param(i2c_bus_nr, int, 0);
MODULE_PARM_DESC(i2c_bus_nr,  "i2c bus number where tuner reside (-1 ==> auto assign)");

static int radio_nr  = -1;
module_param(radio_nr, int, 0);
MODULE_PARM_DESC(radio_nr, "Set radio device number (/dev/radioX).  Default: -1 (autodetect)");

/* Driver state struct */
struct audiocon {
        struct v4l2_device  v4l2_dev;
        struct video_device vdev;
        struct mutex lock;
};

/* audiocon_card is a main structure where all stuff will be keeped */
static struct audiocon twisted_card;

/* ==== CALLBACKS! ====*/
/*
 * v4l2 ioctl call backs.
 * we are just a wrapper for v4l2_sub_devs.
 */

static inline struct v4l2_device *get_v4l2_dev(struct file *file)
{
        return &((struct audiocon *)video_drvdata(file))->v4l2_dev;
}

static int radio_querycap(struct file *file, void *priv, struct v4l2_capability *cap)
{
        strlcpy(cap->driver, "tuner", sizeof(cap->driver));
        strlcpy(cap->card,   "Some tuner card...", sizeof(cap->card));

        snprintf(cap->bus_info, sizeof(cap->bus_info), "platform: audiocon");

        cap->capabilities = V4L2_CAP_TUNER | V4L2_CAP_RADIO;
        return 0;
}

static int radio_g_tuner(struct file *file, void *priv, struct v4l2_tuner *t)
{
        return v4l2_device_call_until_err(get_v4l2_dev(file), 0, tuner, g_tuner, t);
}

static int radio_enum_input(struct file *file, void *priv, struct v4l2_input *i)
{
        if (i->index != 0)
                return -EINVAL;
        strcpy(i->name, "Radio");
        i->type = V4L2_INPUT_TYPE_TUNER;

        return 0;
}

static int radio_g_audio(struct file *file, void *priv, struct v4l2_audio *a)
{
        //if (unlikely(a->index))
        //        return -EINVAL;
        //strcpy(a->name, "Radio");

        a->index = 0;
        strlcpy(a->name, "Radio", sizeof(a->name));
        a->capability = V4L2_AUDCAP_STEREO;
        return 0;
}

static int radio_s_tuner(struct file *file, void *priv, struct v4l2_tuner *t)
{
        return v4l2_device_call_until_err(get_v4l2_dev(file), 0, tuner, s_tuner, t);
}

static int radio_s_audio(struct file *file, void *fh, struct v4l2_audio *a)
{
        return a->index ? -EINVAL : 0;
}

static int radio_s_input(struct file *file, void *fh, unsigned int i)
{
        return i ? -EINVAL : 0;
}

static int radio_queryctrl(struct file *file, void *priv, struct v4l2_queryctrl *qc)
{
        return v4l2_device_call_until_err(get_v4l2_dev(file), 0, core,  queryctrl, qc);
}

static int vidioc_g_ctrl(struct file *file, void *p, struct v4l2_control *vc)
{
        return v4l2_device_call_until_err(get_v4l2_dev(file), 0, core,  g_ctrl, vc);
}

static int vidioc_s_ctrl(struct file *file, void *p, struct v4l2_control *vc)
{
        return v4l2_device_call_until_err(get_v4l2_dev(file), 0, core, s_ctrl, vc);
}

static int vidioc_g_frequency(struct file *file, void *p, struct v4l2_frequency *vf)
{
        return v4l2_device_call_until_err(get_v4l2_dev(file), 0, tuner, g_frequency, vf);
}

static int vidioc_s_frequency(struct file *file, void *p, struct v4l2_frequency *vf)
{
        return v4l2_device_call_until_err(get_v4l2_dev(file), 0, tuner, s_frequency, vf);
}

static int vidioc_g_input(struct file *filp, void *priv,  unsigned int *i)
{
        *i = 0;
        return 0;
}

/* ==== CALLBACKS! ====*/
/* Here we have two structure that are used by endpoint application - like ncurces radio */
static const struct v4l2_ioctl_ops audiocon_ioctl_ops = {
        .vidioc_querycap    = radio_querycap,     
        .vidioc_g_tuner     = radio_g_tuner,
        .vidioc_s_tuner     = radio_s_tuner,
        .vidioc_g_frequency = vidioc_g_frequency, 
        .vidioc_s_frequency = vidioc_s_frequency, 
        .vidioc_g_input     = vidioc_g_input, 
        .vidioc_s_input     = radio_s_input,
        .vidioc_g_audio     = radio_g_audio,      
        .vidioc_s_audio     = radio_s_audio,
        .vidioc_queryctrl   = radio_queryctrl,
        .vidioc_g_ctrl      = vidioc_g_ctrl,      
        .vidioc_s_ctrl      = vidioc_s_ctrl,
        .vidioc_enum_input  = radio_enum_input,   
};

static const struct v4l2_file_operations audiocon_fops = {
        .owner          = THIS_MODULE,
        .unlocked_ioctl = video_ioctl2,
};

/* First method that will be called when module is loaded into kernel space */
static int __init audiocon_init(void)
{
    /*
       Prepare pointer declaration, when & is used then pointer adress is set to given object 
       adapter  - pointer that will be used with given i2c bus 
       core     - pointer that keep adress of audiocon_card 
       v4l2_dev - pointer that keep adress of v4l2_dev structure with is inside audiocon_card (core) or pointer to another pointer ... need check that.
    */
    struct i2c_adapter *adapter;
    struct audiocon *core = &twisted_card;
    struct v4l2_device *v4l2_dev = &core->v4l2_dev; 
    struct v4l2_subdev *sd = NULL;

    PINFO("audiocon_init called for i2c bus number: %d ", i2c_bus_nr);

    strlcpy(v4l2_dev->name, "audiocon", sizeof(v4l2_dev->name));
    mutex_init(&core->lock);

    if (v4l2_device_register(NULL, v4l2_dev) < 0) {
                PWARN("Failed to register v4l2 device.");
                return -EINVAL;
    }

    /* vdev (video_device) need to have name, fops and ioctl_ops */
    strlcpy(core->vdev.name, v4l2_dev->name, sizeof(core->vdev.name));

    core->vdev.v4l2_dev = v4l2_dev;

    core->vdev.fops = &audiocon_fops;
    core->vdev.ioctl_ops = &audiocon_ioctl_ops;

    core->vdev.release = video_device_release_empty;
    core->vdev.lock = &core->lock;

    adapter = i2c_get_adapter(i2c_bus_nr);
    if (!adapter) {
        PWARN("Cannot get i2c adapter %d", i2c_bus_nr);
        return -ENODEV;
    }

    PINFO("Found adapter: %s with number: %d", adapter->name, adapter->nr);

    sd = v4l2_i2c_new_subdev(&core->v4l2_dev, adapter, "tuner", 0, v4l2_i2c_tuner_addrs(ADDRS_RADIO));

    if (IS_ERR_OR_NULL(sd)) {
        PWARN("Failed to register tuner subdevice.");
        i2c_put_adapter(adapter);
        return -ENODEV;
    }

    /* set the private structure that will be used on your driver with video_set_drvdata(). */
    video_set_drvdata(&core->vdev, core);

    if (video_register_device(&core->vdev, VFL_TYPE_RADIO, radio_nr) < 0){
        PWARN("Failed to register video device."); 
        v4l2_device_unregister(v4l2_dev);
        return -EINVAL;
    }

    v4l2_info(v4l2_dev, "V4L2 device registered as %s\n", video_device_node_name(&core->vdev));
    PINFO("Successful initialized audiocon radio interface ;>");
    return 0;
}

static void __exit audiocon_exit(void)
{
   struct audiocon *core = &twisted_card;
   PINFO("audiocon_exit called!");
   video_unregister_device(&core->vdev);
   v4l2_device_unregister(&core->v4l2_dev);
}

module_init(audiocon_init);
module_exit(audiocon_exit);

MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_DESCRIPTION(DRIVER_DESC);
MODULE_LICENSE("GPL");
MODULE_VERSION(DRIVER_VERSION);

